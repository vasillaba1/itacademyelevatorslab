package com.elevator;

import java.util.ArrayList;

public class BuildingWithElevator {

    private ArrayList<User> userList = new ArrayList() {
    };
    private ArrayList<Elevator> elevatorsList = new ArrayList() {
    };

    public BuildingWithElevator() {
    }

    public User createUser(String name, int position) {
        User user = new User(name, position);
        userList.add(user);
        return user;
    }

    public Elevator createElevator(int id, int curentPosition, Direction direction) {
        Elevator elevator = new Elevator(id, curentPosition, direction);
        elevatorsList.add(elevator);
        return elevator;
    }

    public void callElevator(User user, Direction direction, int toFloor) {
        user.setDirection(direction);
        user.setToFloor(toFloor);
        if (check(user)){
            System.out.println("User " + user.getName() + " is alredy on " + user.getToFloor() + " floor");
            return;
        }
        Elevator elevatorToUser = chooseElevator(user);
        System.out.println("User " + user.getName() + " обрав " + elevatorToUser.getId() + " elevator and go on " + user.getToFloor() + " floor");
        startMove(user, elevatorToUser);
    }

    public Elevator chooseElevator(User user) {
        ArrayList<Elevator> chooseList = new ArrayList<>();
        if (user.getDirection() == Direction.UP) {
            if (isLower(user)){
                Elevator[] elevatorArray = addToArray(user);
                return isEqualsDirection(elevatorArray, user);
            }
        }else{
            if (isUpper(user)){
                Elevator[] elevatorArray = addToArray(user);
                return isEqualsDirection(elevatorArray, user);
            }
        }
        return chooseNearest(user);
    }
    private Elevator chooseNearest(User user){
        Elevator rezult = elevatorsList.get(0);
        for(Elevator el : elevatorsList){
            if (Math.abs(el.getCurrentPosition() - user.getPosition()) < Math.abs(rezult.getCurrentPosition() - user.getPosition())){
                rezult = el;
            }
        }
        return rezult;
    }
    private boolean isLower(User user){
        for (Elevator el : elevatorsList) {
            if (el.getCurrentPosition() < user.getPosition()) {
                return true;
            }
        }
        return false;
    }
    private boolean isUpper(User user){
        for (Elevator el : elevatorsList) {
            if (el.getCurrentPosition() > user.getPosition()) {
                return true;
            }
        }
        return false;
    }

    private Elevator[] addToArray(User user){
        ArrayList<Elevator> chooseList = new ArrayList<>();
        if (user.getDirection() == Direction.UP){
            for (Elevator el : elevatorsList) {
                if (el.getCurrentPosition() < user.getPosition()) {
                    chooseList.add(el);
                }
            }
            Elevator[] elevatorsArr = new Elevator[chooseList.size()];
            int k = 0;
            for (Elevator el : chooseList) {
                elevatorsArr[k] = el;
                k++;
            }
            elevatorsArr = sort(elevatorsArr, user);
            return elevatorsArr;
        }else {
            for (Elevator el : elevatorsList) {
                if (el.getCurrentPosition() > user.getPosition()) {
                    chooseList.add(el);
                }
            }
            Elevator[] elevatorsArr = new Elevator[chooseList.size()];
            int k = 0;
            for (Elevator el : chooseList) {
                elevatorsArr[k] = el;
                k++;
            }
            elevatorsArr = sort(elevatorsArr, user);
            return elevatorsArr;
        }
    }

    private Elevator[] sort(Elevator[] elevator, User user){
        for (int j = 0; j < elevator.length - 1; j++) {
            for (int i = 0; i < elevator.length - 1; i++) {
                Elevator swap;
                if (Math.abs(elevator[i].getCurrentPosition() - user.getPosition()) > Math.abs(elevator[i + 1].getCurrentPosition() - user.getPosition())) {
                    swap = elevator[i];
                    elevator[i] = elevator[i + 1];
                    elevator[i + 1] = swap;
                }
            }
        }
        return elevator;
    }

    private Elevator isEqualsDirection(Elevator[] elevators, User user){
        for(Elevator el : elevators){
            if (el.getDirection() == user.getDirection()){
                return el;
            }
        }
        elevators[0].setDirection(user.getDirection());
        return elevators[0];
    }

    public void startMove(User user, Elevator elevator) {
        user.setPosition(user.getToFloor());
        elevator.setCurrentPosition(user.getPosition());
    }
    private boolean check(User user){
        if (user.getPosition() == user.getToFloor()){
            return true;
        }
        return false;
    }


    public void print() {
        System.out.println("Build have " + elevatorsList.size() + " elevators.");
        for (int i = 0; i < elevatorsList.size(); i++) {
            System.out.println("\tElevator № " + (elevatorsList.get(i).getId()) + " is located on " + elevatorsList.get(i).getCurrentPosition());
        }
        System.out.println("In the building is " + userList.size() + " user.");
        for (int i = 0; i < userList.size(); i++) {
            System.out.println("\tUser " + userList.get(i).getName() + " is located on " + userList.get(i).getPosition());
        }

    }
}
