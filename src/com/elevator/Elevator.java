package com.elevator;

import java.util.ArrayList;
import java.util.List;

public class Elevator {
    private int id;
    private int currentPosition;
    private Direction direction;
    private int topFloor;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }
    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public Direction getDirection() {
        return direction;
    }
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getTopFloor() {
        return topFloor;
    }
    public void setTopFloor(int topFloor) {
        this.topFloor = topFloor;
    }


    public Elevator(int id, int currentPosition, Direction direction) {
        setId(id);
        setCurrentPosition(currentPosition);
        setDirection(direction);
    }

}


