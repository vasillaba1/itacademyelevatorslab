package com.elevator;

public class ElevatorMain {
    public static void main(String[] args) {
        BuildingWithElevator build = new BuildingWithElevator();
        Elevator elevator1 = build.createElevator(1,2, Direction.UP);
        Elevator elevator2 = build.createElevator(2,8,Direction.UP);
        Elevator elevator3 = build.createElevator(3,9,Direction.DOWN);
        User user1 = build.createUser("user1", 1);
        User user2 = build.createUser("user2", 10);
        build.callElevator(user1, Direction.UP, 4);
        build.callElevator(user2, Direction.UP, 15);

    }

}
