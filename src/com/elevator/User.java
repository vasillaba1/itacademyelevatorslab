package com.elevator;

public class User {
    private String name;
    private int position;
    private Direction direction;
    private int toFloor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }


    public int getToFloor() {
        return toFloor;
    }

    public void setToFloor(int toFloor) {
        this.toFloor = toFloor;
    }


    public User(String name, int position) {
        setName(name);
        setPosition(position);
    }

}
enum Direction{
    UP,
    DOWN
}
